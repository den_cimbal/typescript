import { getMovies, renderMovies, renderRandomMovie, renderFavoriteMoviesFromLocalStorage } from './utils/movie';
import { IMovie } from './utils/interface';
import { addRemoveFromFavorites } from "./utils/helpers";

const activeCategoryRadio = document.querySelector('input[name=btnradio]:checked') as HTMLElement;
let activeCategory: string = activeCategoryRadio.id;
let searchUrl = '';
let currentPage = 1;
let isFirstLoad = true;

export async function render(): Promise<void> {
    const films = await getMovies(activeCategory) as IMovie[];
    renderMovies(films);

    if(isFirstLoad) {
        isFirstLoad = false;
        renderRandomMovie(films);
        renderFavoriteMoviesFromLocalStorage();
        initEvents();
    }
}

function initEvents(): void {
    const categoryWrapper = document.getElementById('button-wrapper') as HTMLElement;
    const submit = document.getElementById('submit') as HTMLElement;
    const search = document.getElementById('search') as HTMLInputElement;

    categoryWrapper.addEventListener('click', (event: MouseEvent) => {
        const target = event.target as HTMLElement;
        if (target.classList.contains('btn-check')) {
            search.value = '';
            currentPage = 1;
            activeCategory = target.id;
            render();
        }
    });

    const loadMoreButton = document.getElementById('load-more') as HTMLElement;
    loadMoreButton.addEventListener('click', async () => {
        searchUrl = search.value ? 'search' : activeCategory;
        currentPage += 1;
        const films = await getMovies(searchUrl, {page: currentPage, query: search.value}) as IMovie[];
        const isNeedClearNode = false;
        renderMovies(films, isNeedClearNode);
    });

    submit.addEventListener('click', async () => {
        currentPage = 1;
        searchUrl = search.value;
        const url: string = searchUrl ? 'search' : activeCategory;
        const films = await getMovies(url, {query: searchUrl}) as IMovie[];
        renderMovies(films);
    });

    search.addEventListener('keypress', (e) => {
        if(e.key === 'Enter') {
            submit.click();
        }
    });

    document.addEventListener('click', (event: MouseEvent) => {
        const target = event.target as HTMLElement;
        if (target.classList.contains('js-favorites')) {
            const id: number = +(target.dataset.id as string);
            addRemoveFromFavorites(id, target);
        }
    });
}
