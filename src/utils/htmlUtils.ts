export function createHTMLElementFromString(htmlString: string): Node {
    const div = document.createElement('div');
    div.innerHTML = htmlString.trim();

    return div.firstChild as Node;
}

export function clearNode(node: HTMLElement): void {
    node.innerHTML = '';
}