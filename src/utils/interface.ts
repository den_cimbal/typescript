export interface IMovie {
    id: number,
    backdrop_path?: string,
    overview: string,
    release_date?: string,
    title?: string,
    poster_path?: string
}

export interface ISettings {
    [key: string]: boolean | number | string
}