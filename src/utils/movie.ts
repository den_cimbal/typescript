import { API_KEY } from "../config/credentials";
import { IMovie, ISettings } from "./interface";
import { createHTMLElementFromString, clearNode } from "./htmlUtils";
import { isInFavorites } from "./helpers";

const filmContainer = document.getElementById('film-container') as HTMLElement;

const getMovies = async (url: string, settings: ISettings = {}): Promise<IMovie[] | undefined> => {
    try {
        const queryString: string = Object.keys(settings).map(key => key + '=' + settings[key]).join('&');
        const URL: string = url === `search` ? `search/movie` : `movie/${url}`;
        const response: Response = await fetch(`https://api.themoviedb.org/3/${URL}?api_key=${API_KEY}${'&' + queryString}`);
        const { results } = await response.json();

        if (!response.ok) {
            throw new Error("Wrong query");
        }

        const movies: IMovie[] = results.map(({ id, backdrop_path, overview, release_date, poster_path, title }: IMovie) => ({ id, backdrop_path, overview, release_date, poster_path, title }));

        return movies;
    } catch(error) {
        console.log(error.message);
    }
}

function getRandomMovie(movies: IMovie[]): IMovie {
    return movies[Math.floor(Math.random()*movies.length)];
}

function renderRandomMovie(movies: IMovie[]): void {
    const movie = getRandomMovie(movies);
    const container = document.getElementById('random-movie') as HTMLElement;
    container.style.background = `url(https://image.tmdb.org/t/p/original/${movie.backdrop_path || movie.poster_path}) top center / cover no-repeat`;
    const htmlString = `
        <div class="row py-lg-5">
            <div
                class="col-lg-6 col-md-8 mx-auto"
                style="background-color: #2525254f"
            >
                <h1 id="random-movie-name" class="fw-light text-light">${movie.title}</h1>
                <p id="random-movie-description" class="lead text-white">${movie.overview}</p>
            </div>
        </div>
    `;

    clearNode(container);

    container.appendChild(createHTMLElementFromString(htmlString));
}

function renderMovie(movie: IMovie): Node {
    const isFavorite = isInFavorites(movie.id);

    const htmlString = `
        <div class="col-lg-3 col-md-4 col-12 p-2 js-movie-item">
            <div class="card shadow-sm">
                <img
                    src="https://image.tmdb.org/t/p/original/${movie.poster_path}"
                />
                <svg
                    data-id="${movie.id}"
                    xmlns="http://www.w3.org/2000/svg"
                    stroke="red"
                    fill="${isFavorite ? 'red' : 'rgba(255, 0, 0, .3)'}"
                    width="50"
                    height="50"
                    class="bi bi-heart-fill position-absolute p-2 js-favorites"
                    viewBox="0 -2 18 22"
                >
                    <path
                        fill-rule="evenodd"
                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                        style="pointer-events: none;"
                    />
                </svg>
                <div class="card-body">
                    <p class="card-text truncate">${movie.overview}</p>
                    <div
                        class="
                            d-flex
                            justify-content-between
                            align-items-center
                        "
                    >
                        <small class="text-muted">${movie.release_date || ''}</small>
                    </div>
                </div>
            </div>
        </div>
    `;

    return createHTMLElementFromString(htmlString);
}

function renderMovies(movies: IMovie[], isNeedClearNode = true): void {
    if(isNeedClearNode) {
        clearNode(filmContainer);
    }

    const fragmentMovies = document.createDocumentFragment();

    for (let i = 0; i < movies.length; i += 1) {
        const movie = renderMovie(movies[i]);
        fragmentMovies.appendChild(movie);
    }
    filmContainer.appendChild(fragmentMovies);
}

async function getMovieDetails(movieId: number): Promise<IMovie | undefined> {
    try {
        const response: Response = await fetch(`https://api.themoviedb.org/3/movie/${movieId}?api_key=${API_KEY}`);
        const result = await response.json();

        if (!response.ok) {
            throw new Error("Wrong query");
        }

        const { id, overview, release_date, poster_path } = result;
        const movie: IMovie = { id, overview, release_date, poster_path };

        return movie;
    } catch(error) {
        console.log(error.message);
    }
}

function renderFavoriteMovie(movie: IMovie): void {
    const container = document.getElementById('favorite-movies') as HTMLElement;
    const htmlString = `
        <div class="col-12 p-2 js-movie-item">
            <div class="card shadow-sm">
                <img
                    src="https://image.tmdb.org/t/p/original/${movie.poster_path}"
                />
                <svg
                    data-id="${movie.id}"
                    xmlns="http://www.w3.org/2000/svg"
                    stroke="red"
                    fill="red"
                    width="50"
                    height="50"
                    class="bi bi-heart-fill position-absolute p-2 js-favorites"
                    viewBox="0 -2 18 22"
                >
                    <path
                        fill-rule="evenodd"
                        d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
                        style="pointer-events: none;"
                    />
                </svg>
                <div class="card-body">
                    <p class="card-text truncate">${movie.overview}</p>
                    <div
                        class="
                            d-flex
                            justify-content-between
                            align-items-center
                        "
                    >
                        <small class="text-muted">${movie.release_date || ''}</small>
                    </div>
                </div>
            </div>
        </div>
    `;

    container.appendChild(createHTMLElementFromString(htmlString));
}

function renderFavoriteMoviesFromLocalStorage(): void {
    const localStorageArray: number[] = JSON.parse(localStorage.favorites);
    if(localStorageArray.length) {
        localStorageArray.forEach(async (id: number) => {
            const movie = await getMovieDetails(id) as IMovie;
            renderFavoriteMovie(movie);
        });
    }
}

function removeMovieFromLocalStorage(targetId: number): void {
    const favoriteContainer = document.getElementById('favorite-movies') as HTMLElement;
    const favoriteButton = favoriteContainer.querySelector(`[data-id="${targetId}"]`) as HTMLElement;
    const movieToRemove = favoriteButton.closest('.js-movie-item') as HTMLElement;
    const mainMovie = filmContainer.querySelector(`[data-id="${targetId}"]`) as HTMLElement;

    if(mainMovie) {
        mainMovie.setAttribute('fill', 'rgba(255, 0, 0, .3)');
    }

    movieToRemove.remove();
}

export {
    getMovies,
    renderMovies,
    renderRandomMovie,
    getMovieDetails,
    renderFavoriteMovie,
    renderFavoriteMoviesFromLocalStorage,
    removeMovieFromLocalStorage
};
