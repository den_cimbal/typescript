import { getMovieDetails, renderFavoriteMovie, removeMovieFromLocalStorage } from "./movie";
import { IMovie } from "./interface";

export async function addRemoveFromFavorites(id: number, target: HTMLElement): Promise<void> {
    const isFavorite: boolean = isInFavorites(id);
    const localStorageArray: number[] = JSON.parse(localStorage.favorites);

    if(!isFavorite) {
        localStorageArray.push(id);
        localStorage.favorites = JSON.stringify(localStorageArray);
        target.setAttribute('fill', 'red');
        const movie = await getMovieDetails(id) as IMovie;
        renderFavoriteMovie(movie);
    } else {
        const index: number = localStorageArray.findIndex((elem) => elem === id);
        localStorageArray.splice(index, 1);
        localStorage.favorites = JSON.stringify(localStorageArray);
        target.setAttribute('fill', 'rgba(255, 0, 0, .3)');
        removeMovieFromLocalStorage(id);
    }
}

export function isInFavorites(id: number): boolean {
    if(!localStorage.getItem('favorites')) {
        localStorage.setItem('favorites', JSON.stringify([]));
        return false;
    }

    if(JSON.parse(localStorage.favorites).includes(id)) {
        return true;
    }

    return false;
}